package fi.irf.bot.logic;

import fi.irf.bot.message.object.Lane;
import fi.irf.bot.model.Edge;
import fi.irf.bot.model.TrackGraph;
import fi.irf.bot.model.TrackPhysicks;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 23:35
 * To change this template use File | Settings | File Templates.
 */
public class WeightLogic {

    public static void setMaxSpeeds(TrackGraph trackGraph) {
        setMaxSpeedCalculatedFromNextTurns(trackGraph);
        if(trackGraph.getTrackPhysicks().getPositiveAcceleration_x() != 0.0) {
            setMaxGainedSpeedCalculatedFromLastTurns(trackGraph);
        }
    }

    private static void setMaxSpeedCalculatedFromNextTurns(TrackGraph trackGraph) {
        // First set physical maximums to curve pieces
        for (Edge edge : trackGraph.getEdges()) {
            if (edge.isCurve()) {
                double maxSpeedForCurve = getMaxSpeedForCurvePiece(edge, trackGraph);
                edge.setMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn(maxSpeedForCurve);
            }
        }

        // Looppaa palat lopusta alkuun ja jos tarvitaan jarrutusta, niin vähennä maksiminopeutta palan alussa
        // Iteroidaan kaksi kertaa, jotta ensimmäisellä kierroksen jälkeiset nullit tulee hoidettua
        for (int p = 0; p < 2; p++) {
            for (int i = trackGraph.getEdges().size() - 1; i >= 0; i--) {
                Edge currentEdge = trackGraph.getEdges().get(i);
                Edge nextEdge = getNextEdgeOnDestinationLane(currentEdge, trackGraph);
                if (nextEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn() != null) {
                    double currentMaxSpeed = CalculatorUtil.getStartSpeedWithDecclerationLength(nextEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn(), currentEdge.getLength(), trackGraph.getTrackPhysicks());
                    if (currentEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn() == null || currentMaxSpeed < currentEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn()) {
                        currentEdge.setMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn(currentMaxSpeed);
                    }
                }
            }
        }

        // Set weights
        for (int i = trackGraph.getEdges().size() - 1; i >= 0; i--) {
            Edge currentEdge = trackGraph.getEdges().get(i);
            Edge nextEdge = getNextEdgeOnDestinationLane(currentEdge, trackGraph);
            double averageSpeed = (nextEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn() + currentEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn()) / 2;
            double usedTime = (currentEdge.getLength()) / averageSpeed;
            currentEdge.setWeight((int) (usedTime * 100));
        }
    }

    private static Edge getNextEdgeOnDestinationLane(Edge currentEdge, TrackGraph trackGraph) {
        Lane lane = currentEdge.getDestination().getLane();
        for (Edge edge : trackGraph.getEdges()) {
            if(currentEdge.getDestination() == edge.getSource() && edge.getDestination().getLane().getIndex().equals(lane.getIndex())) {
                return edge;
            }
        }
        return null;
    }

    private static Edge getPreviousEdgeOnSourceLane(Edge currentEdge, TrackGraph trackGraph) {
        Lane lane = currentEdge.getSource().getLane();
        for (Edge edge : trackGraph.getEdges()) {
            if(currentEdge.getSource() == edge.getDestination() && edge.getSource().getLane().getIndex().equals(lane.getIndex())) {
                return edge;
            }
        }
        return null;
    }

    private static void setMaxGainedSpeedCalculatedFromLastTurns(TrackGraph trackGraph) {
        // First round with max speeds calculated from NEXT turns
        for (int i = trackGraph.getEdges().size() - 1; i >= 0; i--) {
            Edge currentEdge = trackGraph.getEdges().get(i);
            Edge previousEdge = getPreviousEdgeOnSourceLane(currentEdge, trackGraph);

            double startSpeed = previousEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn();
            double length = previousEdge.getLength();

            if(previousEdge.isCurve()) {
                currentEdge.setMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn(previousEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn());
            } else {
                double maxSpeed = CalculatorUtil.getMaxSpeedAtEndOfPiece(startSpeed, length, trackGraph.getTrackPhysicks());
                currentEdge.setMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn(maxSpeed);
            }
        }

        // Second round with max speeds calculated with accelerations
        for (int i = trackGraph.getEdges().size() - 1; i >= 0; i--) {
            Edge currentEdge = trackGraph.getEdges().get(i);
            Edge previousEdge = getPreviousEdgeOnSourceLane(currentEdge, trackGraph);

            double startSpeed = previousEdge.getMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn();
            double length = previousEdge.getLength();

            if(previousEdge.isCurve()) {
                currentEdge.setMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn(previousEdge.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn());
            } else {
                double maxSpeed = CalculatorUtil.getMaxSpeedAtEndOfPiece(startSpeed, length, trackGraph.getTrackPhysicks());
                currentEdge.setMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn(maxSpeed);
            }
        }
    }

    private static double getMaxSpeedForCurvePiece(Edge edge, TrackGraph trackGraph) {
        // Keskipakoisvoima F = m*v^2/r
        TrackPhysicks trackPhysicks = trackGraph.getTrackPhysicks();

        // FIXME : This commented row in physically correct
        //double speed = Math.sqrt(trackPhysicks.getMaxForceInCurves() * edge.getRadius());
        double speed = Math.sqrt(trackPhysicks.getMaxForceInCurves() * edge.getPieceRadius());

        return speed;
    }

}
