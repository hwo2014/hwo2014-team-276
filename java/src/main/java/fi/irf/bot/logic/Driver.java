package fi.irf.bot.logic;

import fi.irf.bot.message.CarPositions;
import fi.irf.bot.message.object.*;
import fi.irf.bot.message.outgoing.Ping;
import fi.irf.bot.message.outgoing.SendMsg;
import fi.irf.bot.message.outgoing.SwitchLane;
import fi.irf.bot.message.outgoing.Throttle;
import fi.irf.bot.model.Edge;
import fi.irf.bot.model.TrackGraph;
import fi.irf.bot.model.Vertex;
import fi.irf.bot.shortestpath.DijkstraAlgorithm;
import lombok.Data;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:48
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Driver {

    private enum BreakTest {
        NOT_STARTED, ACCELERATED, BREAKED, DONE;
    }

    private Integer totalLaps = 3;
    private CarId myCar;
    private TrackGraph trackGraph;
    private Integer lastGameTick;
    private Edge lastEdge;
    private Double currentThrottle;
    private CarPosition myLastPosition;
    private BreakTest breakTest = BreakTest.NOT_STARTED;
    private Double lastVelocity = null;
    private Double lastAcceleration = null;

    private List<Edge> currentPath;

    private double staticThrottle = 0.5;




    public Driver(CarId myCar, TrackGraph trackGraph, LinkedList<Vertex> path) {
        this.myCar = myCar;
        this.trackGraph = trackGraph;
        this.currentPath = this.getEdgePath(path);
    }

    public SendMsg getNextMove(CarPositions carPositions) {
        int gameTick = 0;
        if(carPositions.getGameTick() != null) {
            gameTick = carPositions.getGameTick();
        }

        /*
        PARSE CAR POSITIONS
         */
        SendMsg message = null;
        List<CarPosition> otherCarPositions = new ArrayList<>();
        CarPosition myCarPosition = null;
        for (CarPosition carPosition : carPositions.getData()) {
            if(carPosition.getId().equals(this.myCar)) {
                myCarPosition = carPosition;
            } else {
                otherCarPositions.add(carPosition);
            }
        }
        Integer currentLap = myCarPosition.getPiecePosition().getLap() + 1;
        boolean isLastLap = currentLap != null && currentLap.equals(this.totalLaps);

        /*
        DETERMINE CURRENT EDGE
         */
        Edge currentEdge = this.getCurrentEdgeOnPath(myCarPosition);
        if(currentEdge == null) {
            // Gets here after goal line
            currentEdge = this.getCurrentEdgeOutsidePath(myCarPosition);
            this.updateOptimalPath(currentEdge, isLastLap);
        }

        /*
        UPDATE PHYSICS
         */
        Double velocity = this.calculateVelocity(currentEdge, carPositions.getGameTick(), myCarPosition);
        Double acceleration = null;
        if(this.lastVelocity != null && velocity != null && gameTick > 0) {
            acceleration = (velocity - this.lastVelocity) / (gameTick - this.lastGameTick);
            //System.out.println("velocity: " + velocity + "  acceleration: " + acceleration);
        }
        if(velocity != null && myCarPosition.getAngle() != 0.0 && myLastPosition != null) {
            double angleChange = Math.abs(this.myLastPosition.getAngle() - myCarPosition.getAngle());
            if(angleChange > 0 && currentEdge.getRadius() != null) {
                this.updateSlidingFriction(velocity, angleChange, currentEdge.getRadius());
            }
        }
        if(lastEdge != currentEdge) {
            this.updateOptimalPath(currentEdge, isLastLap);
        }

        /*
        FIND OUT NEXT EDGE
         */
        Edge nextEdge = this.getNextEdge(currentEdge);


        /*
        CHECK WHETHER NEEDS TURNING
         */
        if(lastEdge != currentEdge) {
            // Check if needs to turn on this edge
            SwitchLane.Direction direction = this.getTurn(nextEdge);
            if(direction != null) {
                message = new SwitchLane(direction);
            }
        }

        /*
        BREAKTEST AT THE START
         */
        if(message == null) {
            if(breakTest != BreakTest.DONE) {
                if(gameTick > 6 && this.breakTest == BreakTest.BREAKED) {
                    double velocityDifference = velocity - this.lastVelocity;
                    double decceleration = velocityDifference / (gameTick - this.lastGameTick);
                    this.trackGraph.getTrackPhysicks().setNegativeAccelerationPerTick(Math.abs(decceleration));
                    this.updateOptimalPath(currentEdge, isLastLap);
                    this.breakTest = BreakTest.DONE;
                    message = new Throttle(1.0);
                } else {
                    if(gameTick > 4 && this.breakTest == BreakTest.ACCELERATED) {
                        this.breakTest = BreakTest.BREAKED;
                        message = new Throttle(0.0);
                    } else if (this.breakTest == BreakTest.NOT_STARTED){
                        this.breakTest = BreakTest.ACCELERATED;
                        message = new Throttle(1.0);
                    } else if (gameTick > 2 && this.breakTest == BreakTest.ACCELERATED) {
                        if(this.lastAcceleration != null && acceleration != null) {
                            CalculatorUtil.setPositiveAccelerationFactors(this.lastVelocity, velocity, this.lastAcceleration, acceleration, this.trackGraph.getTrackPhysicks());
                        }
                    }
                }
            }
        }

        /*
        UPDATE SPEED
         */
        if(message == null) {
            if(velocity != null) {
                message = new Throttle(this.getThrottle(velocity, currentEdge, nextEdge, myCarPosition));
            }
        }

        this.lastGameTick = gameTick;
        this.lastEdge = currentEdge;
        this.myLastPosition = myCarPosition;
        this.lastVelocity = velocity;
        this.lastAcceleration = acceleration;

        if(message == null) {
            message = new Ping();
        }

        return message;
    }

    private double getThrottle(Double currentVelocity, Edge currentEdge, Edge nextEdge, CarPosition myCarPosition) {
        Double targetSpeed = nextEdge.getMaxSpeedAtTheStartOfPiece();

        // TODO : Does not work well with curves
        Double distanceToEndOfCurrentEdge = currentEdge.getLength() - myCarPosition.getPiecePosition().getInPieceDistance();

        Double deccelerationLength = CalculatorUtil.getDeccelerationLength(currentVelocity, targetSpeed, this.trackGraph.getTrackPhysicks());
        if(deccelerationLength <= distanceToEndOfCurrentEdge) {
            return 0.0;
        } else {
            return 1.0;
        }
    }

    private void updateOptimalPath(Edge currentEdge, boolean isLastLap) {
        // Tämän voisi tehdä myös niin, että reitti etsitään aina edelliseen palaseen saakka - paitsi viimeisellä kierroksella


        // Do not update path on goal piece
        for (Vertex goalVertex : this.trackGraph.getGoalVertexes()) {
            if(currentEdge.getDestination() == goalVertex) {
                return;
            }
        }

        if(trackGraph.getTrackPhysicks().getPositiveAcceleration_x() != 0.0) {
            System.out.println("valmis");
        }

        WeightLogic.setMaxSpeeds(this.trackGraph);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(trackGraph);
        dijkstra.execute(currentEdge.getDestination());

        Double totalWeight = null;
        List<Edge> optimalPath = null;
        List<Edge> lastPath = null; // Only for debugging

        List<Vertex> targetVertexes;
        if(isLastLap) {
            targetVertexes = this.trackGraph.getGoalVertexes();
        } else {
            targetVertexes = new ArrayList<>();
            Piece targetPiece = currentEdge.getSource().getPiece();
            for(Lane lane : targetPiece.getFromVertexes().keySet()) {
                targetVertexes.add(targetPiece.getFromVertexes().get(lane));
            }
        }

        for(Vertex goalVertex : targetVertexes) {
            LinkedList<Vertex> path = dijkstra.getPath(goalVertex);
            List<Edge> edgePath = this.getEdgePath(path);
            Double pathWeight = this.getTimeToDrivePath(edgePath);

            if(optimalPath == null) {
                optimalPath = edgePath;
                totalWeight = pathWeight;
            } else {
                if(pathWeight < totalWeight) {
                    totalWeight = pathWeight;
                    optimalPath = edgePath;
                }
            }
            System.out.println("path weight: " + pathWeight);
            lastPath = edgePath;
        }

        // Add currentEdge source to optimalPath
        optimalPath.add(0, currentEdge);

        this.currentPath = optimalPath;
    }

    private void updateSlidingFriction(double velocity, double angle, double radius) {
        double force = Math.pow(velocity, 2) / radius;
        //System.out.println("force: " + force);
    }

    private Double calculateVelocity(Edge currentEdge, Integer currentGameTick, CarPosition myCarPosition) {
        if(currentGameTick == null) {
            return 0.0;
        }

        int time = currentGameTick;
        if(this.lastGameTick != null) {
            time -= this.lastGameTick;
        }

        double distanceInLastPiece = 0.0;
        double distanceInCurrentPiece = 0.0;
        if(this.myLastPosition != null) {
            if(myCarPosition.getPiecePosition().getPieceIndex() == this.myLastPosition.getPiecePosition().getPieceIndex()) {
                distanceInCurrentPiece = myCarPosition.getPiecePosition().getInPieceDistance() - this.myLastPosition.getPiecePosition().getInPieceDistance();
            } else {
                return null;
            }
        } else {
            distanceInCurrentPiece = myCarPosition.getPiecePosition().getInPieceDistance();
        }



        /*
        double distanceInLastPiece = 0.0;
        double distanceInCurrentPiece = 0.0;
        if(this.myLastPosition != null) {
            if(myCarPosition.getPiecePosition().getPieceIndex() != this.myLastPosition.getPiecePosition().getPieceIndex()) {
                if(this.lastEdge.isCurve()) {
                    double percents = (this.lastEdge.getPositionLength()- this.myLastPosition.getPiecePosition().getInPieceDistance()) / this.lastEdge.getPositionLength();
                    distanceInLastPiece = this.lastEdge.getLength() * percents;
                } else {
                    distanceInLastPiece = this.lastEdge.getLength() - this.myLastPosition.getPiecePosition().getInPieceDistance();
                }
                if(currentEdge.isCurve()) {
                    double percents =  myCarPosition.getPiecePosition().getInPieceDistance() / currentEdge.getPositionLength();
                    distanceInCurrentPiece = currentEdge.getLength() * percents;
                } else {
                    distanceInCurrentPiece = myCarPosition.getPiecePosition().getInPieceDistance();
                }
            } else {
                if(currentEdge.isCurve()) {
                    double percents =  (myCarPosition.getPiecePosition().getInPieceDistance() - this.myLastPosition.getPiecePosition().getInPieceDistance()) / currentEdge.getPositionLength();
                    distanceInCurrentPiece = currentEdge.getLength() * percents;
                } else {
                    distanceInCurrentPiece = myCarPosition.getPiecePosition().getInPieceDistance() - this.myLastPosition.getPiecePosition().getInPieceDistance();
                }
            }
        } else {
            if(currentEdge.isCurve()) {
                double percents =  myCarPosition.getPiecePosition().getInPieceDistance() / currentEdge.getPositionLength();
                distanceInCurrentPiece = currentEdge.getLength() * percents;
            } else {
                distanceInCurrentPiece = myCarPosition.getPiecePosition().getInPieceDistance();
            }
        }*/

        double velocity = (distanceInLastPiece + distanceInCurrentPiece) / time;

        return velocity;
    }

    private Edge getNextEdge(Edge currentEdge) {
        for(int i=0; i<this.currentPath.size(); i++) {
            Edge edge = this.currentPath.get(i);
            if(edge == currentEdge) {
                int nextIndex = i+1;
                if(nextIndex == this.currentPath.size()) {
                    nextIndex = 0;
                }
                return this.currentPath.get(nextIndex);
            }
        }
        return null;
    }

    private SwitchLane.Direction getTurn(Edge edge) {
        Lane sourceLane = edge.getSource().getLane();
        Lane targetLane = edge.getDestination().getLane();
        if(sourceLane.getIndex() != targetLane.getIndex()) {
            //System.out.println("yritetaan kaantya!!!");
            if(sourceLane.getIndex() > targetLane.getIndex()) {
                return SwitchLane.Direction.LEFT;
            } else {
                return SwitchLane.Direction.RIGHT;
            }
        } else {
            return null;
        }
    }

    private Edge getCurrentEdgeOnPath(CarPosition carPosition) {
        PiecePosition piecePosition = carPosition.getPiecePosition();
        int pieceIndex = piecePosition.getPieceIndex();
        for (Edge edge : this.currentPath) {
            if(edge.getSource().getPiece().getPieceIndex().equals(pieceIndex)) {
                if(edge.getDestination().getLane().getIndex().equals(carPosition.getPiecePosition().getLane().getEndLaneIndex())) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getCurrentEdgeOutsidePath(CarPosition carPosition) {
        PiecePosition piecePosition = carPosition.getPiecePosition();
        int pieceIndex = piecePosition.getPieceIndex();
        for (Edge edge : this.getTrackGraph().getEdges()) {
            if(edge.getSource().getPiece().getPieceIndex().equals(pieceIndex)) {
                if(edge.getDestination().getLane().getIndex().equals(carPosition.getPiecePosition().getLane().getEndLaneIndex())) {
                    return edge;
                }
            }
        }
        // Never gets here
        return null;
    }

    private double getTimeToDrivePath(List<Edge> edgePath) {
        double totalWeight = 0.0;
        for (Edge edge : edgePath) {
            totalWeight += edge.getWeight();
        }
        return totalWeight;
    }

    private List<Edge> getEdgePath(LinkedList<Vertex> path) {
        List<Edge> edgePath = new ArrayList<>();
        for(int i=0; i<path.size() - 1; i++) {
            Vertex source = path.get(i);
            Vertex destination = path.get(i+1);
            boolean found = false;
            for (Edge edge : this.trackGraph.getEdges()) {
                if(edge.getSource() == source && edge.getDestination() == destination) {
                    edgePath.add(edge);
                    found = true;
                    break;
                }
            }
            if(!found) {
                System.out.println("heps");
            }
        }
        return edgePath;
    }

}
