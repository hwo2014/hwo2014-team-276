package fi.irf.bot.logic;

import com.google.gson.Gson;
import fi.irf.bot.message.ExampleMessages;
import fi.irf.bot.message.GameInitWrapper;
import fi.irf.bot.message.object.Lane;
import fi.irf.bot.message.object.Piece;
import fi.irf.bot.message.object.Track;
import fi.irf.bot.model.Edge;
import fi.irf.bot.model.TrackGraph;
import fi.irf.bot.model.Vertex;
import fi.irf.bot.shortestpath.DijkstraAlgorithm;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */
public class TrackCreator {

    public static void main(String[] args) {
        Gson gson = new Gson();
        GameInitWrapper gameInitWrapper = gson.fromJson(ExampleMessages.gameInitMessage2, GameInitWrapper.class);
        Track track = gameInitWrapper.getData().getRace().getTrack();
        TrackGraph trackGraph = TrackCreator.createTrackGraph(track);
        WeightLogic.setMaxSpeeds(trackGraph);

        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(trackGraph);
        dijkstra.execute(trackGraph.getVertexes().get(0));

        LinkedList<Vertex> path = dijkstra.getPath(trackGraph.getVertexes().get(trackGraph.getVertexes().size() - 2));

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

        System.out.println("valmis");
    }

    public static TrackGraph createTrackGraph(Track track) {
        // Set piece indexes
        for (int i=0; i<track.getPieces().size(); i++) {
            Piece piece = track.getPieces().get(i);
            piece.setPieceIndex(i);
        }

        List<Vertex> vertexes = new ArrayList<>();
        List<Edge> edges = new ArrayList<>();

        String[] laneNames = new String[]{"A", "B", "C", "D", "E", "F", "G", "H"};

        for (int i=0; i<track.getPieces().size(); i++) {
            Piece piece = track.getPieces().get(i);
            Map<Lane, Vertex> fromVertexes = new HashMap<>();
            for (int j=0; j<track.getLanes().size(); j++) {
                Lane lane = track.getLanes().get(j);
                String vertexId = null;
                if(j < laneNames.length) {
                    vertexId = laneNames[j] + i;
                } else {
                    vertexId = i + "";
                }
                Vertex vertex = new Vertex(vertexId, lane, piece);
                vertexes.add(vertex);
                fromVertexes.put(lane, vertex);
            }
            piece.setFromVertexes(fromVertexes);
        }

        for (int i=0; i<track.getPieces().size(); i++) {
            int nextPieceIndex = i+1;
            if(nextPieceIndex == track.getPieces().size()) {
                nextPieceIndex = 0;
            }

            Piece piece = track.getPieces().get(i);
            Piece nextPiece = track.getPieces().get(nextPieceIndex);

            // Set to vertexes to pieces
            piece.setToVertexes(nextPiece.getFromVertexes());

            if(piece.getIsSwitch()) {
                for (Lane lane : track.getLanes()) {
                    for (Lane anotherLane : track.getLanes()) {
                        Vertex fromVertex = piece.getFromVertexes().get(lane);
                        Vertex toVertex = piece.getToVertexes().get(anotherLane);
                        Edge edge = createEdge(piece, fromVertex, toVertex);
                        edges.add(edge);
                    }
                }
            } else {
                for (Lane lane : track.getLanes()) {
                    Vertex fromVertex = piece.getFromVertexes().get(lane);
                    Vertex toVertex = piece.getToVertexes().get(lane);
                    Edge edge = createEdge(piece, fromVertex, toVertex);
                    edges.add(edge);
                }
            }
        }

        TrackGraph trackGraph = new TrackGraph(vertexes, edges);

        Piece goalPiece = track.getPieces().get(track.getPieces().size() - 1);
        for(Lane lane : goalPiece.getToVertexes().keySet()) {
            trackGraph.getGoalVertexes().add(goalPiece.getToVertexes().get(lane));
        }

        return trackGraph;
    }

    private static Edge createEdge(Piece piece, Vertex fromVertex, Vertex toVertex) {
        Edge edge = new Edge(fromVertex, toVertex);
        CalculatorUtil.setLengthToEdge(edge, piece);
        return edge;
    }



}
