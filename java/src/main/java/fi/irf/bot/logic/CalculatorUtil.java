package fi.irf.bot.logic;

import fi.irf.bot.message.object.Lane;
import fi.irf.bot.message.object.Piece;
import fi.irf.bot.model.Edge;
import fi.irf.bot.model.TrackPhysicks;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 10:30
 * To change this template use File | Settings | File Templates.
 */
public class CalculatorUtil {

    public static void setLengthToEdge(Edge edge, Piece piece) {
        if(piece.isCurve()) {
            if(piece.getIsSwitch() && edge.getSource().getLane().getIndex() != edge.getDestination().getLane().getIndex()) {
                Lane lane = edge.getSource().getLane();
                double angle = piece.getAngle();
                Integer edgeRadius = null;
                if(angle > 0) {
                    // Turns right
                    //edgeRadius = piece.getRadius() + lane.getDistanceFromCenter();
                    edgeRadius = piece.getRadius() - lane.getDistanceFromCenter();
                } else {
                    // Turns left
                    //edgeRadius = piece.getRadius() - lane.getDistanceFromCenter();
                    edgeRadius = piece.getRadius() + lane.getDistanceFromCenter();
                }
                Double curveLength = calculateCurveLength(edgeRadius, angle);
                Double positionLength = calculateCurveLength(piece.getRadius(), angle);
                edge.setLength(curveLength);
                edge.setAngle(angle);
                edge.setRadius(edgeRadius);
                edge.setPieceRadius(piece.getRadius());
                edge.setPositionLength(positionLength);



            } else {
                Lane lane = edge.getSource().getLane();
                double angle = piece.getAngle();
                Integer edgeRadius = null;
                if(angle > 0) {
                    // Turns right
                    //edgeRadius = piece.getRadius() + lane.getDistanceFromCenter();
                    edgeRadius = piece.getRadius() - lane.getDistanceFromCenter();
                } else {
                    // Turns left
                    //edgeRadius = piece.getRadius() - lane.getDistanceFromCenter();
                    edgeRadius = piece.getRadius() + lane.getDistanceFromCenter();
                }
                Double curveLength = calculateCurveLength(edgeRadius, angle);
                Double positionLength = calculateCurveLength(piece.getRadius(), angle);
                edge.setLength(curveLength);
                edge.setAngle(angle);
                edge.setRadius(edgeRadius);
                edge.setPieceRadius(piece.getRadius());
                edge.setPositionLength(positionLength);
            }
        } else {
            if(piece.getIsSwitch() && edge.getSource().getLane().getIndex() != edge.getDestination().getLane().getIndex()) {
                Lane sourceLane = edge.getSource().getLane();
                Lane destinationLane = edge.getDestination().getLane();
                double tangetLength = 0;
                if(sourceLane.getIndex() > destinationLane.getIndex()) {
                    tangetLength = Math.abs(sourceLane.getDistanceFromCenter() - destinationLane.getDistanceFromCenter());
                } else {
                    tangetLength = Math.abs(destinationLane.getDistanceFromCenter() - sourceLane.getDistanceFromCenter());
                }
                double hypotenusa = Math.sqrt(Math.pow(tangetLength, 2.0) + Math.pow((double)piece.getLength(), 2.0));
                edge.setLength(hypotenusa);
                edge.setPositionLength(hypotenusa);
            } else {
                edge.setLength((double)piece.getLength());
                edge.setPositionLength((double)piece.getLength());
            }
        }
    }

    public static double calculateCurveLength(int radius, Double angle) {
        return (Math.abs(angle)/360.0*2*radius*Math.PI);
    }

    // s = v0 * (v-v0) / a + 1/2*(v-v0)^2/a
    public static double getDeccelerationLength(double startSpeed, double endSpeed, TrackPhysicks trackPhysicks) {
       double deccelerationLength = startSpeed * (endSpeed - startSpeed) / trackPhysicks.getNegativeAccelerationPerTick()
               + 0.5 * Math.pow(endSpeed - startSpeed, 2)/trackPhysicks.getNegativeAccelerationPerTick();
        return deccelerationLength;
    }

    // v0 = (vˆ2+a*s)/v
    public static double getStartSpeedWithDecclerationLength(double endSpeed, double length, TrackPhysicks trackPhysicks) {
        return (Math.pow(endSpeed, 2)+trackPhysicks.getNegativeAccelerationPerTick()*length) / endSpeed;
    }

    public static double getMaxSpeedAtEndOfPiece(double startSpeed, double length, TrackPhysicks trackPhysicks) {
        // TODO : hieman yksinkertaistettu. todellisuudessa kiihtyvyys vauhtelee nopeuden funktiona. tässä käytetään alkunopeutta koko matkalla
        // Voisi esim jakaa palasen kymmeneen osaan, niin saisi tarkemman aproksimaation
        return Math.sqrt(Math.pow(startSpeed, 2.0) + 2*(trackPhysicks.getPositiveAcceleration_x()*startSpeed + trackPhysicks.getPositiveAcceleration_c())*length);
    }

/*
    public static void setNegativeAccelerationFactors(double lastVelocity, double currentVelocity, double lastAcceleration, double currentAcceleration, TrackPhysicks trackPhysicks) {
        double x = (currentAcceleration - lastAcceleration) / (currentVelocity - lastVelocity);
        double c = currentAcceleration - (x * currentVelocity);
        trackPhysicks.setNegativeAcceleration_x(x);
        trackPhysicks.setNegativeAcceleration_c(c);
    }
*/
    public static void setPositiveAccelerationFactors(double lastVelocity, double currentVelocity, double lastAcceleration, double currentAcceleration, TrackPhysicks trackPhysicks) {
        double x = (currentAcceleration - lastAcceleration) / (currentVelocity - lastVelocity);
        double c = currentAcceleration - (x * currentVelocity);
        trackPhysicks.setPositiveAcceleration_x(x);
        trackPhysicks.setPositiveAcceleration_c(c);
    }

}
