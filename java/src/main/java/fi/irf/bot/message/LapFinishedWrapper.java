package fi.irf.bot.message;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 23:52
 * To change this template use File | Settings | File Templates.
 */
@Data
public class LapFinishedWrapper extends AbstractTickMessage<LapFinished> {
}
