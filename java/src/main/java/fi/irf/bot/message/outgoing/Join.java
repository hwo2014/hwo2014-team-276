package fi.irf.bot.message.outgoing;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class Join extends SendMsg {

    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }

}
