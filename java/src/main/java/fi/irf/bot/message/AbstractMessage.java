package fi.irf.bot.message;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
@Data
public class AbstractMessage<T extends Object> {

    private T data;

}
