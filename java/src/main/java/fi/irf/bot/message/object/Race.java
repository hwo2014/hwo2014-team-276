package fi.irf.bot.message.object;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Race {

    private Track track;
    private List<Car> cars;
    private RaceSession raceSession;

}
