package fi.irf.bot.message.outgoing;

import com.google.gson.Gson;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();

}
