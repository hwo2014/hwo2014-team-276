package fi.irf.bot.message.object;

import com.google.gson.annotations.SerializedName;
import fi.irf.bot.model.Vertex;
import lombok.*;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@EqualsAndHashCode
public class Piece {

    private Integer length;

    @SerializedName("switch")
    private Boolean isSwitch = false;
    private Integer radius;
    private Double angle;
    private Integer pieceIndex;

    private Map<Lane, Vertex> fromVertexes;
    private Map<Lane, Vertex> toVertexes;

    public boolean isCurve() {
        if(this.radius != null) {
            return true;
        } else {
            return false;
        }
    }

}
