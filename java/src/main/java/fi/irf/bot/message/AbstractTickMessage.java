package fi.irf.bot.message;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 23:21
 * To change this template use File | Settings | File Templates.
 */
@Data
public class AbstractTickMessage<T extends Object> extends AbstractMessage<T> {

    private String gameId;

    private Integer gameTick;

}
