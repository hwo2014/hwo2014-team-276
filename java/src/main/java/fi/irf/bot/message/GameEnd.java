package fi.irf.bot.message;

import fi.irf.bot.message.object.*;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
@Data
public class GameEnd {

    private List<Result> results;
    private List<BestLap> bestLaps;

}