package fi.irf.bot.message.outgoing;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class CreateRace extends SendMsg {

    public final String trackName;
    public final String password;
    public final Integer carCount;
    public final BotId botId;


    public CreateRace(final String botName, final String botKey, final String trackName,
                      final String password, final int carCount) {
        this.botId = new BotId(botName, botKey);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }

    public static class BotId {
        public final String name;
        public final String key;

        public BotId(final String name, final String key) {
            this.name = name;
            this.key = key;
        }
    }
}
