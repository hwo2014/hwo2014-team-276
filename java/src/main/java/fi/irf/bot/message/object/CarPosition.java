package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:19
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CarPosition {

    private CarId id;

    private Double angle;

    private PiecePosition piecePosition;

}
