package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:27
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PiecePosition {

    private Integer pieceIndex;

    private Double inPieceDistance;

    private PiecePositionLane lane;

    private Integer lap;

}
