package fi.irf.bot.message;

import fi.irf.bot.message.object.Race;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
@Data
public class GameInit {

    private Race race;

}
