package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Result {

    private CarId car;
    private RaceTime result;

}
