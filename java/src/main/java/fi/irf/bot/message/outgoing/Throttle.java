package fi.irf.bot.message.outgoing;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
