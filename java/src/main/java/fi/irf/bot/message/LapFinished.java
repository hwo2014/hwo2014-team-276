package fi.irf.bot.message;

import fi.irf.bot.message.object.CarId;
import fi.irf.bot.message.object.LapTime;
import fi.irf.bot.message.object.RaceTime;
import fi.irf.bot.message.object.Ranking;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
@Data
public class LapFinished {

    private CarId car;

    private LapTime lapTime;

    private RaceTime raceTime;

    private Ranking ranking;
}

