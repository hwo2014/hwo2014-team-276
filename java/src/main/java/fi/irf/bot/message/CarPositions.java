package fi.irf.bot.message;

import com.google.gson.annotations.SerializedName;
import fi.irf.bot.message.object.CarPosition;
import fi.irf.bot.message.object.Race;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:23
 * To change this template use File | Settings | File Templates.
 */
@Data
public class CarPositions extends AbstractTickMessage<List<CarPosition>> {

}

