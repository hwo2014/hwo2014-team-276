package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:49
 * To change this template use File | Settings | File Templates.
 */
@Data
public class RaceTime {

    private Integer laps;
    private Integer ticks;
    private Integer millis;


}
