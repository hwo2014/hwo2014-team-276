package fi.irf.bot.message.outgoing;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class SwitchLane extends SendMsg {

    public enum Direction {
        LEFT("Left"), RIGHT("Right");
        String name;
        Direction(String name) {
            this.name = name;
        }

    }

    private Direction direction;

    public SwitchLane(Direction direction) {
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return this.direction.name;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

}
