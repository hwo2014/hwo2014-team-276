package fi.irf.bot.message.object;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
@Data
@EqualsAndHashCode(of = "index")
public class Lane {

    Integer distanceFromCenter;
    Integer index;
}
