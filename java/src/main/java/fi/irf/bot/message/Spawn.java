package fi.irf.bot.message;

import com.google.gson.annotations.SerializedName;
import fi.irf.bot.message.object.CarId;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 23:10
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Spawn {

    @SerializedName("data")
    private CarId car;

    private String gameId;

    private Integer gameTick;

}
