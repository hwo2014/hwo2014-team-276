package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:56
 * To change this template use File | Settings | File Templates.
 */
@Data
public class BestLap {

    private CarId car;
    private LapTime result;

}
