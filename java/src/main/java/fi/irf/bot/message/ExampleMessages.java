package fi.irf.bot.message;

import com.google.gson.Gson;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:19
 * To change this template use File | Settings | File Templates.
 */
public class ExampleMessages {

    public static void main(String[] args) {
        Gson gson = new Gson();
        GameInitWrapper gameInitWrapper = gson.fromJson(ExampleMessages.gameInitMessage, GameInitWrapper.class);
        CarPositions positions = gson.fromJson(ExampleMessages.carPositionsMessage, CarPositions.class);
        GameEndWrapper gameEndWrapper = gson.fromJson(ExampleMessages.gameEndMessage, GameEndWrapper.class);
        YourCar yourCar = gson.fromJson(ExampleMessages.yourCarMessage, YourCar.class);
        Crash crash = gson.fromJson(ExampleMessages.crashMessage, Crash.class);
        Spawn spawn = gson.fromJson(ExampleMessages.spawnMessage, Spawn.class);
        DnfWrapper dnfWrapper = gson.fromJson(ExampleMessages.dnfMessage, DnfWrapper.class);
        Finish finish = gson.fromJson(ExampleMessages.finishMessage, Finish.class);

        LapFinishedWrapper lapFinishedWrapper = gson.fromJson(ExampleMessages.lapFinishedMessage, LapFinishedWrapper.class);

        System.out.println("parsed");
    }

    public static String carPositionsMessage = "{\n" +
            "  \"msgType\": \"carPositions\",\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"id\": {\n" +
            "        \"name\": \"Schumacher\",\n" +
            "        \"color\": \"red\"\n" +
            "      },\n" +
            "      \"angle\": 0,\n" +
            "      \"piecePosition\": {\n" +
            "        \"pieceIndex\": 0,\n" +
            "        \"inPieceDistance\": 0,\n" +
            "        \"lane\": {\n" +
            "          \"startLaneIndex\": 0,\n" +
            "          \"endLaneIndex\": 0\n" +
            "        },\n" +
            "        \"lap\": 0\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": {\n" +
            "        \"name\": \"Rosberg\",\n" +
            "        \"color\": \"blue\"\n" +
            "      },\n" +
            "      \"angle\": 45,\n" +
            "      \"piecePosition\": {\n" +
            "        \"pieceIndex\": 0,\n" +
            "        \"inPieceDistance\": 20,\n" +
            "        \"lane\": {\n" +
            "          \"startLaneIndex\": 1,\n" +
            "          \"endLaneIndex\": 1\n" +
            "        },\n" +
            "        \"lap\": 0\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 0\n" +
            "}";

    public static String gameInitMessage = "{\n" +
            "  \"msgType\": \"gameInit\",\n" +
            "  \"data\": {\n" +
            "    \"race\": {\n" +
            "      \"track\": {\n" +
            "        \"id\": \"indianapolis\",\n" +
            "        \"name\": \"Indianapolis\",\n" +
            "        \"pieces\": [\n" +
            "          {\n" +
            "            \"length\": 100\n" +
            "          },\n" +
            "          {\n" +
            "            \"length\": 100,\n" +
            "            \"switch\": true\n" +
            "          },\n" +
            "          {\n" +
            "            \"radius\": 200,\n" +
            "            \"angle\": 22.5\n" +
            "          }\n" +
            "        ],\n" +
            "        \"lanes\": [\n" +
            "          {\n" +
            "            \"distanceFromCenter\": -20,\n" +
            "            \"index\": 0\n" +
            "          },\n" +
            "          {\n" +
            "            \"distanceFromCenter\": 0,\n" +
            "            \"index\": 1\n" +
            "          },\n" +
            "          {\n" +
            "            \"distanceFromCenter\": 20,\n" +
            "            \"index\": 2\n" +
            "          }\n" +
            "        ],\n" +
            "        \"startingPoint\": {\n" +
            "          \"position\": {\n" +
            "            \"x\": -340,\n" +
            "            \"y\": -96\n" +
            "          },\n" +
            "          \"angle\": 90\n" +
            "        }\n" +
            "      },\n" +
            "      \"cars\": [\n" +
            "        {\n" +
            "          \"id\": {\n" +
            "            \"name\": \"Schumacher\",\n" +
            "            \"color\": \"red\"\n" +
            "          },\n" +
            "          \"dimensions\": {\n" +
            "            \"length\": 40,\n" +
            "            \"width\": 20,\n" +
            "            \"guideFlagPosition\": 10\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"id\": {\n" +
            "            \"name\": \"Rosberg\",\n" +
            "            \"color\": \"blue\"\n" +
            "          },\n" +
            "          \"dimensions\": {\n" +
            "            \"length\": 40,\n" +
            "            \"width\": 20,\n" +
            "            \"guideFlagPosition\": 10\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"raceSession\": {\n" +
            "        \"laps\": 3,\n" +
            "        \"maxLapTimeMs\": 30000,\n" +
            "        \"quickRace\": true\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public static String gameInitMessage2 = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"TomisBot_0.51\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"32668b17-8681-497e-b59f-6d838182d9ac\"}";

    public static String gameEndMessage = "{\n" +
            "  \"msgType\": \"gameEnd\",\n" +
            "  \"data\": {\n" +
            "    \"results\": [\n" +
            "      {\n" +
            "        \"car\": {\n" +
            "          \"name\": \"Schumacher\",\n" +
            "          \"color\": \"red\"\n" +
            "        },\n" +
            "        \"result\": {\n" +
            "          \"laps\": 3,\n" +
            "          \"ticks\": 9999,\n" +
            "          \"millis\": 45245\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"car\": {\n" +
            "          \"name\": \"Rosberg\",\n" +
            "          \"color\": \"blue\"\n" +
            "        },\n" +
            "        \"result\": {\n" +
            "          \n" +
            "        }\n" +
            "      }\n" +
            "    ],\n" +
            "    \"bestLaps\": [\n" +
            "      {\n" +
            "        \"car\": {\n" +
            "          \"name\": \"Schumacher\",\n" +
            "          \"color\": \"red\"\n" +
            "        },\n" +
            "        \"result\": {\n" +
            "          \"lap\": 2,\n" +
            "          \"ticks\": 3333,\n" +
            "          \"millis\": 20000\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"car\": {\n" +
            "          \"name\": \"Rosberg\",\n" +
            "          \"color\": \"blue\"\n" +
            "        },\n" +
            "        \"result\": {\n" +
            "          \n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";

    public static String yourCarMessage = "{\n" +
            "  \"msgType\": \"yourCar\",\n" +
            "  \"data\": {\n" +
            "    \"name\": \"Schumacher\",\n" +
            "    \"color\": \"red\"\n" +
            "  }\n" +
            "}";

    public static String crashMessage = "{\n" +
            "  \"msgType\": \"crash\",\n" +
            "  \"data\": {\n" +
            "    \"name\": \"Rosberg\",\n" +
            "    \"color\": \"blue\"\n" +
            "  },\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 3\n" +
            "}";

    public static String spawnMessage = "{\n" +
            "  \"msgType\": \"spawn\",\n" +
            "  \"data\": {\n" +
            "    \"name\": \"Rosberg\",\n" +
            "    \"color\": \"blue\"\n" +
            "  },\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 150\n" +
            "}";


    public static String lapFinishedMessage = "{\n" +
            "  \"msgType\": \"lapFinished\",\n" +
            "  \"data\": {\n" +
            "    \"car\": {\n" +
            "      \"name\": \"Schumacher\",\n" +
            "      \"color\": \"red\"\n" +
            "    },\n" +
            "    \"lapTime\": {\n" +
            "      \"lap\": 1,\n" +
            "      \"ticks\": 666,\n" +
            "      \"millis\": 6660\n" +
            "    },\n" +
            "    \"raceTime\": {\n" +
            "      \"laps\": 1,\n" +
            "      \"ticks\": 666,\n" +
            "      \"millis\": 6660\n" +
            "    },\n" +
            "    \"ranking\": {\n" +
            "      \"overall\": 1,\n" +
            "      \"fastestLap\": 1\n" +
            "    }\n" +
            "  },\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 300\n" +
            "}";

    private static String dnfMessage = "{\n" +
            "  \"msgType\": \"dnf\",\n" +
            "  \"data\": {\n" +
            "    \"car\": {\n" +
            "      \"name\": \"Rosberg\",\n" +
            "      \"color\": \"blue\"\n" +
            "    },\n" +
            "    \"reason\": \"disconnected\"\n" +
            "  },\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 650\n" +
            "}";

    private static String finishMessage = "{\n" +
            "  \"msgType\": \"finish\",\n" +
            "  \"data\": {\n" +
            "    \"name\": \"Schumacher\",\n" +
            "    \"color\": \"red\"\n" +
            "  },\n" +
            "  \"gameId\": \"OIUHGERJWEOI\",\n" +
            "  \"gameTick\": 2345\n" +
            "}";

}
