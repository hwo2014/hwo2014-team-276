package fi.irf.bot.message;

import fi.irf.bot.message.object.CarId;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 23:37
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Dnf {

    private CarId car;

    private String reason;

}
