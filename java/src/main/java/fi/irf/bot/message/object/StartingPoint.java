package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:33
 * To change this template use File | Settings | File Templates.
 */
@Data
public class StartingPoint {

    private Position position;
    private Double angle;

}
