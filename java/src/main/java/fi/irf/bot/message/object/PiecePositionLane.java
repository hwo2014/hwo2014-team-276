package fi.irf.bot.message.object;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 22:30
 * To change this template use File | Settings | File Templates.
 */
@Data
public class PiecePositionLane {

    private Integer startLaneIndex;

    private Integer endLaneIndex;


}
