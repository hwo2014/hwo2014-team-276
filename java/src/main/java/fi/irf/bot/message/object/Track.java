package fi.irf.bot.message.object;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 15/04/14
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
@Data
public class Track {

    private String id;
    private String name;
    private List<Piece> pieces;
    private List<Lane> lanes;
    private StartingPoint startingPoint;

}
