package fi.irf.bot.message.outgoing;

/**
 * Created by mrantane on 17.4.2014.
 */
public class JoinRace extends CreateRace {
    public JoinRace(String botName, String botKey, String trackName, String password, int carCount) {
        super(botName, botKey, trackName, password, carCount);
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}
