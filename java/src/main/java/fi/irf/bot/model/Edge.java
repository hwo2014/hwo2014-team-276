package fi.irf.bot.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 22:24
 * To change this template use File | Settings | File Templates.
 *
 * Kuvaa radan palasessa olevia aukkoja palasen alkupisteistä loppupisteisiin.
 *
 */
@Getter
@Setter
@EqualsAndHashCode
public class Edge {

    private Integer radius;
    private Integer pieceRadius;
    private Double angle;
    private Double length;
    private Double positionLength;
    private Vertex source;
    private Vertex destination;
    private double weight;
    private Double maxSpeedAtTheStartOfPieceCalculatedFromNextTurn;
    private Double maxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn;

    public Edge(Vertex source, Vertex destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Edge{" +
                source + "->"
                 + destination +
                ", weight=" + weight +
                ", maxSpeedFromNextTurn=" + maxSpeedAtTheStartOfPieceCalculatedFromNextTurn +
                ", maxGainedSpeedFromLastTurn=" + maxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn +
                '}';
    }


    public boolean isCurve() {
        return this.angle != null;
    }

    public Double getMaxSpeedAtTheStartOfPiece() {
        Double targetSpeed1 = this.getMaxSpeedAtTheStartOfPieceCalculatedFromNextTurn();
        Double targetSpeed2 = this.getMaxGainedSpeedAtTheStartOfPieceCalculatedFromLastTurn();

        Double targetSpeed = targetSpeed1;
        if(targetSpeed2 != null && targetSpeed2 < targetSpeed1) {
            targetSpeed = targetSpeed2;
        }
        return targetSpeed;
    }
}
