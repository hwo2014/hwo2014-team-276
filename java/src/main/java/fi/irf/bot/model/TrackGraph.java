package fi.irf.bot.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 22:23
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@EqualsAndHashCode
public class TrackGraph {

    private List<Vertex> goalVertexes = new ArrayList<>();
    private TrackPhysicks trackPhysicks = new TrackPhysicks();
    private List<Vertex> vertexes;
    private List<Edge> edges;

    public TrackGraph(List<Vertex> vertexes, List<Edge> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

}
