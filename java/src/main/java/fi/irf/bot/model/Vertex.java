package fi.irf.bot.model;

import fi.irf.bot.message.object.Lane;
import fi.irf.bot.message.object.Piece;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 16/04/14
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 *
 * Kuvaa solmukohtaa radan palasessa. Jokaisessa radan palassa on yhtä monta Vertex-olioita kuin radalla on kaistoja.
 *
 */
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Vertex {

    final String id;
    final Lane lane;
    final Piece piece;

    public Vertex(String id, Lane lane, Piece piece) {
        this.id = id;
        this.lane = lane;
        this.piece = piece;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "id='" + id + '\'' +
                '}';
    }
}
