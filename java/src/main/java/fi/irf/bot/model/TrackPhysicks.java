package fi.irf.bot.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * User: Laurell
 * Date: 17/04/14
 * Time: 10:42
 * To change this template use File | Settings | File Templates.
 */
@Data
public class TrackPhysicks {

    private double maxAngleInCurves = 45.0;

    // Vastustaa mutkassa autoa heittämästä perää
    private double maxForceInCurves = 0.4;

    // Vaikuttaa suoralla kuinka kovaa voi autoa kiihdyttää
    // NÄyttää siltä, että pito on maximaalinen. Ainakin mitä kovempaa painaa kaasua, niin sitä kovempaa auto kiihtyy.
    private double staticFriction = 0.2;

    // Auton hidastuvuus yhden tickin aikana, kun kaasu on nollassa
    // a = (v2 - v1) / (t2-t1)
    private double negativeAccelerationPerTick = 0.1;

    // Jarrutuksessa ei ole kulmakerrointa, joten tätä ei tarvita
    //private double negativeAcceleration_x = 0.0;
    //private double negativeAcceleration_c = 0.0;

    // Acceleration is in format A = xV + c
    private double positiveAcceleration_x = 0.0;
    private double positiveAcceleration_c = 0.0;

}
