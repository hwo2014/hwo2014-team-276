package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.Socket;
import java.util.LinkedList;

import com.google.gson.Gson;
import fi.irf.bot.logic.Driver;
import fi.irf.bot.logic.TrackCreator;
import fi.irf.bot.logic.WeightLogic;
import fi.irf.bot.message.*;
import fi.irf.bot.message.object.CarId;
import fi.irf.bot.message.object.Track;
import fi.irf.bot.message.outgoing.*;
import fi.irf.bot.model.TrackGraph;
import fi.irf.bot.model.Vertex;
import fi.irf.bot.shortestpath.DijkstraAlgorithm;

public class Main {

    public static String hostUrl = "testserver.helloworldopen.com";
    public static int hostPorts = 8091;
    public static String botkey = "jc4vnBrtQ6sL7A";

    private TrackGraph trackGraph;
    private CarId myCar;
    private Driver driver;

    public static void main(String... args) throws IOException {
        /*String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];*/

        String host = hostUrl;
        int port = hostPorts;
        String botName = "TomisBot";
        String botKey = botkey;

        BigDecimal maxForceInCurves = new BigDecimal("0.5902");
        for (int i=0; i<100; i++) {
            maxForceInCurves = maxForceInCurves.add(new BigDecimal("-0.0001"));

            System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            final Socket socket = new Socket(host, port);
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

            new Main(reader, writer, new Join(botName + "_" + maxForceInCurves.toString(), botKey), maxForceInCurves.doubleValue());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, double parameter) throws IOException {
        System.out.println("Kisa starttaa_ + " + parameter);
        this.writer = writer;
        String line = null;

        send(join);
        while((line = reader.readLine()) != null) {
            System.out.println("Tuli viesti: " + line);
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                CarPositions carPositions = gson.fromJson(line, CarPositions.class);
                send(this.driver.getNextMove(carPositions));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                GameInitWrapper gameInitWrapper = gson.fromJson(line, GameInitWrapper.class);
                Track track = gameInitWrapper.getData().getRace().getTrack();
                this.trackGraph = TrackCreator.createTrackGraph(track);
                WeightLogic.setMaxSpeeds(this.trackGraph);
                DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(trackGraph);
                dijkstra.execute(trackGraph.getVertexes().get(0));
                // TODO : pitää olla vaihtoehtoinen tapa mennä maaliian toiselta kaistalta.
                LinkedList<Vertex> path = dijkstra.getPath(trackGraph.getVertexes().get(trackGraph.getVertexes().size() - 2));
                this.driver = new Driver(this.myCar, this.trackGraph, path);
                this.driver.getTrackGraph().getTrackPhysicks().setMaxForceInCurves(parameter);
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println("Tuli viesti: " + line);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                YourCar carWrapper = gson.fromJson(line, YourCar.class);
                this.myCar = carWrapper.getData();
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                break;
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}